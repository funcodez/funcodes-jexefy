# README #

> "The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers."

## What is this repository for? ##

This repository hosts the [`jEXEfy`](https://bitbucket.org/funcodez/funcodes-jexefy)
tool, which is a [`Rust`](https://www.rust-lang.org) program compiling to a binary
(aka `EXE` on Windows) launcher for directly running an appended Java `JAR` on
Windows and Linux.

## Usage ##

The [`jEXEfy`](https://bitbucket.org/funcodez/funcodes-jexefy) launcher actually
starts a `JVM` with itself(!) as `JAR` file (`java -jar <myself>`). The `JVM` then
seeks that provided `JAR` file  (being the [`jEXEfy`](https://bitbucket.org/funcodez/funcodes-jexefy) 
launcher together with the actual `JAR` file appended) until it finds the magic 
bytes identifying the actual `JAR` archive's content, which then gets executed by
the `JVM`. Java system properties and command line arguments are properly taken 
care of by the [`jEXEfy`](https://bitbucket.org/funcodez/funcodes-jexefy) 
launcher.

> For the launcher to run, you must either have a `JRE` or a `JDK` installed and set up in your [`path`](https://en.wikipedia.org/wiki/PATH_(variable)) environment variable or a valid [`JAVA_HOME`](https://stackoverflow.com/questions/2025290/what-is-java-home-how-does-the-jvm-find-the-javac-path-stored-in-java-home) environment variable set up pointing to the `JRE` or the `JDK` in question.

```
$ cat jexefy-x86_64-windows.launcher myapp.jar > myapp.exe
$ chown "$USER" myapp.exe
$ chmod ugo+x myapp.exe
```

The above script concatenates the [`jEXEfy`](https://bitbucket.org/funcodez/funcodes-jexefy) 
launcher with your `JAR` in question (which must be an [executable](https://stackoverflow.com/questions/5258159/how-to-make-an-executable-jar-file)
[`FatJAR`](https://stackoverflow.com/questions/19150811/what-is-a-fat-jar)) 
resulting in an executable `*.exe` file which can be started directly on Windows
(and accordingly on Linux)! The first line achieves the concatenation, lines two 
and three outline changing the file's ownership as well as setting the executable
flags (when using Windows you may use [`Cygwin`](https://www.cygwin.com/) or the
[`WSL`](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux) for this task). 

> Targeting `Linux`, use `jexefy-x86_64-linux.launcher` instead of `jexefy-x86_64-windows.launcher` and `myapp` instead of `myapp.exe`.

## Getting started ##

To get up and running, clone the [`funcodes-jexefy`](https://bitbucket.org/funcodez/funcodes-jexefy/) repository from [`bitbucket`](https://bitbucket.org/funcodez/funcodes-jexefy)'s `git` repository. 

## How do I get set up? ##

Make sure you have the [`Rust`](https://www.rust-lang.org) toolchain installed. Specifically you will need the following:

```
$ rustup target add x86_64-pc-windows-gnu
$ rustup toolchain install stable-x86_64-pc-windows-gnu
$ # rustup target add x86_64-unknown-linux-gnu
$ # rustup toolchain install stable-x86_64-unknown-linux-gnu
$ rustup target add x86_64-unknown-linux-musl
$ rustup toolchain install stable-x86_64-unknown-linux-musl
```

> For `Linux` we use `x86_64-unknown-linux-musl` instead of `x86_64-unknown-linux-gnu` as the former one uses a statically linkable substitute of the `glibc` library, making the resulting `Linux` launcher more cross distribution compatible. 

Then build as follows:

```
$ ./make.sh
```

The binary files are found in the folder `./target` (e.g. `jexefy-x86_64-linux.launcher` for Linux as well as `jexefy-x86_64-windows.launcher` for Windows).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-jexefy/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
