# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being coveFG_RED by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	printf '%*s' "${COLUMNS:-$(tput cols)}" '' | tr ' ' ${char}
}

# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh -h"
	printLn
	echo -e "Build a Linux and Windows JAR launcher to which the actual JAR file just needs to be appended."
	printLn
	echo -e "${ESC_BOLD}-h${ESC_RESET}: Print this help"
	printLn
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
	printHelp
	exit 0
fi

function build {
	printLn
	local target=$1
	local extension=$2
	local targetName="jexefy${extension}"
	local targetDir="${SCRIPT_PATH}/target/${target}/release"
	local targetPath="${targetDir}/${targetName}"
	local finalDir="${SCRIPT_PATH}/target"
	local finalName="jexefy-${target}.launcher"
	finalName=${finalName//\-gnu/}
	finalName=${finalName//\-musl/}
	finalName=${finalName//\-pc/}
	finalName=${finalName//\-unknown/}
	local finalPath="${finalDir}/${finalName}"
	echo -e "> Building <${ESC_BOLD}jexefy${ESC_RESET}> for target <${ESC_BOLD}${target}${ESC_RESET}>..."
	cd "${SCRIPT_PATH}"
	result=$(cargo build --target ${target} --release 2>&1)
	if (($? != 0)); then
		echo "$result"
		echo -e "> ${ESC_BOLD}Failed to build <${finalPath}>, aborting!${ESC_RESET}" 1>&2;
		exit 1
	fi
	if [ ! -f ${targetPath} ]; then
		echo -e "> ${ESC_BOLD}Failed to build <${targetName}> in <${targetDir}>!${ESC_RESET}" 1>&2;
		exit 1
	fi
	cp "${targetPath}" "${finalPath}"
	if (($? != 0)); then
		echo -e "> ${ESC_BOLD}Failed to copy <${finalName}> to <${finalDir}>!${ESC_RESET}" 1>&2;
		exit 1
	fi
	echo -e "> Created <${ESC_BOLD}${finalName}${ESC_RESET}> in <${ESC_BOLD}${finalDir}${ESC_RESET}>!"
	cd "${CURRENT_PATH}"
}

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

build "x86_64-pc-windows-gnu" ".exe"
build "x86_64-unknown-linux-musl"
# build "x86_64-unknown-linux-gnu"
