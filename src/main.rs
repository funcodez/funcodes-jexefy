// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

use std::env;
use std::process::Command;
use std::io::ErrorKind;
use std::path::Path;

fn main() {
	let command;
	if cfg!(windows) {
		command = "java.exe";
	}
	else {
		command = "java";
	}
	let status;
	let mut args: Vec<String> = env::args().collect();
	let name = args.remove(0); // The file-path of the launcher
	let mut sys_args: Vec<String> = Vec::new();
	sys_args.push("-Djdk.gtk.version=2".to_string());
	let mut app_args: Vec<String> = Vec::new();
	for arg in args.iter() {
		if arg.starts_with("-D") {
			sys_args.push(arg.to_string());	
		}
		else {
			app_args.push(arg.to_string());
		}
	}
	match env::current_exe() {
		Ok(exe_path) => {
			match exe_path.to_str() {
				None => panic!("Failed to get current executable's <{}> (?!?) path: {}", name, exe_path.display()),
				Some(s) => {
					status = Command::new( command ).args(&sys_args).args(&["-jar", s]).args(&app_args).status().unwrap_or_else(|error|
					// status = Command::new( command ).args(&sys_args).args(&["-server"]).args(&["-jar", s]).args(&app_args).status().unwrap_or_else(|error|
						{
							if error.kind() == ErrorKind::NotFound {
								match env::var("JAVA_HOME") {
									Ok(java_home) => {
										let path = Path::new(&java_home).join(&"bin").join(&command);
										let status2 = Command::new( path ).args(&sys_args).args(&["-jar", s]).args(&app_args).status().unwrap_or_else(|error|
										// let status2 = Command::new( path ).args(&sys_args).args(&["-server"]).args(&["-jar", s]).args(&app_args).status().unwrap_or_else(|error|
											{
												if error.kind() == ErrorKind::NotFound {
													panic!("Java command <{}> not found, make sure you installed a valid JRE or JDK and set the <path> or <JAVA_HOME> environment varibale correctly: {:?}", command, error);
												} 
												else {
													panic!("Problems occurred launching Java command <{}>, make sure you installed a valid JRE or JDK and set the <path> or <JAVA_HOME> environment varibale correctly: {:?}", command, error);
												}
											}
										);
										match status2.code() {
											Some(code) => std::process::exit( code ),
											None => std::process::exit( -1 )
										}
									},
									Err(e) => panic!("Failed to get current executable's <{}> (?!?) path: {}", name, e)
								};
							} 
							else {
								panic!("Problems occurred launching Java command <{}>, make sure you installed a valid JRE or JDK and set the <path> or <JAVA_HOME> environment varibale correctly: {:?}", command, error);
							}
						}
					);
					match status.code() {
						Some(code) => std::process::exit( code ),
						None => std::process::exit( -1 )
					}
				}
			}
		},
		Err(e) => panic!("Failed to get current executable's <{}> (?!?) path: {}", name, e)
	};
}
